-- database: gestion
-- schema: schgestion
-- user: postgres
-- port: 5432
-- host: localhost
-- password: 12345
ALTER USER "postgres" SET search_path = "schgestion";

--------------------------------------------------------------------CREANDO TABLAS
CREATE TABLE schgestion.colaboradores
(
	id SERIAL,
	codigo CHARACTER VARYING(50) NOT NULL,
	nombre CHARACTER VARYING(100) NOT NULL
)
ALTER TABLE schgestion.colaboradores ADD CONSTRAINT pk_colaboradores PRIMARY KEY (codigo);

CREATE TABLE schgestion.tareas
(
	id SERIAL,
	descripcion CHARACTER VARYING(100) NOT NULL,
	colaborador CHARACTER VARYING(50) NOT NULL,
	estado CHARACTER VARYING(50) NOT NULL,
	prioridad CHARACTER VARYING(50) NOT NULL,
	fechainicio TIMESTAMP WITHOUT TIME ZONE NOT NULL,
	fechafin TIMESTAMP WITHOUT TIME ZONE NOT NULL,
	notas VARCHAR
)
ALTER TABLE schgestion.tareas ADD CONSTRAINT pk_tareas PRIMARY KEY (id);
ALTER TABLE schgestion.tareas ADD CONSTRAINT fk_tareas_colaboradores FOREIGN KEY (colaborador) REFERENCES schgestion.colaboradores(codigo); 

--------------------------------------------------------------------INSERTANDO COLABORADORES POR DEFECTO
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c0', 'Sin Asignar');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c1', 'Diego Castillo');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c2', 'Luisa Rojas');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c3', 'Roxana Vargas');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c4', 'Dennis Arias');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c5', 'Juan Lopez');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c6', 'Carlos Salas');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c7', 'Daniel Sanchez');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c8', 'Karla Villalobos');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c9', 'Katherine Alfaro');
INSERT INTO schgestion.colaboradores (codigo, nombre) VALUES ('c10', 'Julio Acosta');

--------------------------------------------------------------------FUNCIONES
CREATE OR REPLACE FUNCTION obtener_colaboradores()
RETURNS TABLE(id integer, codigo character varying, nombre character varying) AS $$
    BEGIN
         RETURN QUERY
             SELECT colaboradores.id, colaboradores.codigo, colaboradores.nombre
             FROM schgestion.colaboradores;
    END;
$$ LANGUAGE plpgsql;
 
--SELECT * FROM schgestion.obtener_colaboradores(); 

CREATE FUNCTION schgestion.crear_tarea(pDescripcion character varying, pColaborador character varying, pEstado character varying,
							pPrioridad character varying, pFecha_inicio timestamp, pFecha_fin timestamp, pNotas character varying)
  RETURNS void AS
  $BODY$
      BEGIN
        INSERT INTO tareas(descripcion, colaborador, estado, prioridad, fechainicio, fechafin, notas)
        VALUES(pDescripcion, pColaborador, pEstado, pPrioridad, pFecha_inicio, pFecha_fin, pNotas);
      END;
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
  
--SELECT * FROM schgestion.crear_tarea('Prueba', 'c0','PENDIENTE','ALTA', '07/03/2020 10:40:00','08/03/2020 20:00:00', '');


CREATE OR REPLACE FUNCTION schgestion.obtener_tareas()
RETURNS TABLE(id integer, descripcion character varying, colaborador character varying, estado character varying, prioridad character varying,
			 fechainicio timestamp, fechafin timestamp, notas  character varying) AS $$
    BEGIN
         RETURN QUERY
             SELECT t.id, t.descripcion, c.nombre as colaborador, t.estado, t.prioridad, t.fechainicio, t.fechafin, t.notas  
			 FROM schgestion.tareas t
			 JOIN schgestion.colaboradores c
			 ON c.codigo = t.colaborador
			 ORDER BY fechainicio ASC;
    END;
$$ LANGUAGE plpgsql;
--SELECT * FROM schgestion.obtener_tareas(); 

CREATE OR REPLACE FUNCTION schgestion.borrar_tarea(pId integer)
RETURNS Void AS $$
BEGIN
DELETE FROM schgestion.tareas WHERE id = pId;
END;
$$ LANGUAGE plpgsql VOLATILE;
--SELECT * FROM schgestion.borrar_tarea(3);

CREATE OR REPLACE FUNCTION schgestion.actualizar_tarea(pId integer, pDescripcion character varying, pColaborador character varying, 
													   pEstado character varying, pPrioridad character varying, pFechaInicio timestamp, 
													   pFehcaFin timestamp, pNotas character varying)   
RETURNS void AS 
$BODY$
BEGIN
    UPDATE schgestion.tareas
    SET descripcion = pDescripcion,
		colaborador = pColaborador, 
        estado = pEstado, 
        prioridad = pPrioridad, 
        fechainicio = pFechaInicio, 
        fechafin = pFehcaFin, 
        notas = pNotas
    WHERE   id = pId; 
END;
$BODY$   
LANGUAGE plpgsql VOLATILE   
COST 100;
--SELECT * FROM schgestion.actualizar_tarea(16, 'Cambio5', 'c3','EN PROCESO','ALTA', '09/03/2020 10:40:00','09/03/2020 20:00:00', 'Cambio5');
--------------------------------------------------------------------
--SELECT * FROM schgestion.obtener_tareas(); 
--------------------------------------------------------------------
CREATE OR REPLACE FUNCTION schgestion.filtrar(pColaborador character varying, pEstado character varying, pPrioridad character varying,
											 pFechainicio timestamp, pFechafin timestamp)
RETURNS TABLE(id integer, descripcion character varying, colaborador character varying, estado character varying, prioridad character varying,
			 fechainicio timestamp, fechafin timestamp, notas  character varying) AS $$
    BEGIN
         RETURN QUERY
             SELECT t.id, t.descripcion, c.nombre AS colaborador, t.estado, t.prioridad, t.fechainicio, t.fechafin, t.notas
			 FROM schgestion.tareas t
			 JOIN schgestion.colaboradores c
			 ON c.codigo = t.colaborador
			 WHERE c.nombre LIKE pColaborador OR t.estado LIKE pEstado OR t.prioridad LIKE pPrioridad 
			 OR  (t.fechainicio >= pFechainicio AND t.fechafin <= pFechafin)
			 ORDER BY t.fechainicio ASC;
    END;
$$ LANGUAGE plpgsql;



--*****Omitir*****
--SELECT * FROM schgestion.filtrar('Sin Asignar', '-', '-', '09-03-2020 00:00:00', '12-03-2020 24:00:00');
--SELECT t.id, t.descripcion, c.nombre AS colaborador, t.estado, t.prioridad, t.fechainicio, t.fechafin, t.notas
--FROM schgestion.tareas t
--JOIN schgestion.colaboradores c
--ON c.codigo = t.colaborador
--WHERE c.nombre LIKE '%Sin Asignar%' AND t.estado LIKE '%EN PROCESO%' AND t.prioridad LIKE '%ALTA%' 
--OR  (t.fechainicio >='09-03-2020 00:00:00' AND t.fechafin <= '12-03-2020 24:00:00')
--ORDER BY t.fechainicio ASC;


















