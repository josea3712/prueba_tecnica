﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;//Trabajar con datos
using System.Data.OleDb;
using System.Xml;
using Npgsql;//Para controlar la conexion a base de datos con postgresql
using NpgsqlTypes;
using appGestion.Entity;

namespace appGestion.Data
{
    class ColaboradorD
    {
        AccesoDatosPostgre cnx;

        public string ErrorMsg { get; set; }

        public bool Error { get; set; }
        
        private void reset()
        {
            this.Error = false;
            this.ErrorMsg = "";
        }

        public ColaboradorD()
        {
            this.cnx = AccesoDatosPostgre.Instance;
            this.reset();
        }

        public List<ColaboradorE> obtenerColaboradores() 
        {
            this.reset();
            List<ColaboradorE> colaboradores = new List<ColaboradorE>(); 
            String sql = "SELECT * FROM " + this.cnx.Schema + "obtener_colaboradores()";
            DataSet datos = this.cnx.ejecutarFuncion(sql);
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;

            if (!this.Error)
            {
                foreach (DataRow fila in datos.Tables[0].Rows)
                {
                    ColaboradorE oColaboradorE = new ColaboradorE(
                                                   Int32.Parse(fila["id"].ToString()),
                                                   fila["codigo"].ToString(),
                                                   fila["nombre"].ToString()
                                                   );
                    colaboradores.Add(oColaboradorE);
                }
            }

            return colaboradores;
        }

        



    }
}
