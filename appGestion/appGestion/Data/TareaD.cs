﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;//Trabajar con datos
using System.Data.OleDb;
using System.Xml;
using Npgsql;//Para controlar la conexion a base de datos con postgresql
using NpgsqlTypes;
using appGestion.Entity;

namespace appGestion.Data
{
    class TareaD
    {
        AccesoDatosPostgre cnx;

        public string ErrorMsg { get; set; }

        public bool Error { get; set; }

        private void reset()
        {
            this.Error = false;
            this.ErrorMsg = "";
        }

        public TareaD()
        {
            this.cnx = AccesoDatosPostgre.Instance;
            this.reset();
        }

        public void crearTarea(TareaE pTareaE) 
        {
            this.reset();

            string sql = "SELECT * FROM " + this.cnx.Schema + "crear_tarea(@descripcion, @colaborador, @estado, @prioridad, @fechainicio, @fechafin, @notas)";
          
            Parametro parametros = new Parametro();
            parametros.agregarParametro("@descripcion", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.descripcion);
            parametros.agregarParametro("@colaborador", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.colaborador);
            parametros.agregarParametro("@estado", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.estado);
            parametros.agregarParametro("@prioridad", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.prioridad);
            parametros.agregarParametro("@fechainicio", NpgsqlTypes.NpgsqlDbType.Timestamp, pTareaE.fechaInicio);
            parametros.agregarParametro("@fechafin", NpgsqlTypes.NpgsqlDbType.Timestamp, pTareaE.fechaFin);
            parametros.agregarParametro("@notas", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.notas);

            this.cnx.ejecutarSQL(sql, parametros.obtenerParametros());
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;
        }

        public List<TareaE> obtenerTareas()
        {
            this.reset();
            List<TareaE> tareas = new List<TareaE>();
            string sql = "SELECT * FROM " + this.cnx.Schema + "obtener_tareas()";
            DataSet datos = this.cnx.ejecutarFuncion(sql);
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;

            if (!this.Error)
            {
                foreach (DataRow fila in datos.Tables[0].Rows)
                {
                    TareaE oTareaE = new TareaE(
                                                   Int32.Parse(fila["id"].ToString()),
                                                   fila["descripcion"].ToString(),
                                                   fila["colaborador"].ToString(),
                                                   fila["estado"].ToString(),
                                                   fila["prioridad"].ToString(),
                                                   DateTime.Parse(fila["fechainicio"].ToString()),
                                                   DateTime.Parse(fila["fechafin"].ToString()),
                                                   fila["notas"].ToString()
                                                   );
                    tareas.Add(oTareaE);
                }
            }

            return tareas;
        }

        public void borrarTarea(TareaE pTarea) 
        {
            this.reset();
            string sql = "SELECT * FROM " + this.cnx.Schema + "borrar_tarea(@id)";
            Parametro parametros = new Parametro();
            parametros.agregarParametro("@id", NpgsqlTypes.NpgsqlDbType.Integer, pTarea.id);

            this.cnx.ejecutarSQL(sql, parametros.obtenerParametros());
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;

        }

        public void modificarTarea(TareaE pTareaE)
        {
            this.reset();

            string sql = "SELECT * FROM " + this.cnx.Schema + "actualizar_tarea(@id, @descripcion, @colaborador, @estado, @prioridad, @fechainicio, @fechafin, @notas)";

            Parametro parametros = new Parametro();

            parametros.agregarParametro("@id", NpgsqlTypes.NpgsqlDbType.Integer, pTareaE.id);
            parametros.agregarParametro("@descripcion", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.descripcion);
            parametros.agregarParametro("@colaborador", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.colaborador);
            parametros.agregarParametro("@estado", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.estado);
            parametros.agregarParametro("@prioridad", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.prioridad);
            parametros.agregarParametro("@fechainicio", NpgsqlTypes.NpgsqlDbType.Timestamp, pTareaE.fechaInicio);
            parametros.agregarParametro("@fechafin", NpgsqlTypes.NpgsqlDbType.Timestamp, pTareaE.fechaFin);
            parametros.agregarParametro("@notas", NpgsqlTypes.NpgsqlDbType.Varchar, pTareaE.notas);

            this.cnx.ejecutarSQL(sql, parametros.obtenerParametros());
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;
        }

        public List<TareaE> filtrarTareas(object[] filtroDatos) 
        {
            this.reset();
            List<TareaE> tareas = new List<TareaE>();
            
            string sql = "SELECT * FROM " + this.cnx.Schema + "filtrar(@colaborador, @estado, @prioridad, @fechainicio, @fechafin)";
            Parametro parametros = new Parametro();

            parametros.agregarParametro("@colaborador", NpgsqlTypes.NpgsqlDbType.Varchar, filtroDatos[0].ToString().Trim());
            parametros.agregarParametro("@estado", NpgsqlTypes.NpgsqlDbType.Varchar, filtroDatos[1].ToString().Trim());
            parametros.agregarParametro("@prioridad", NpgsqlTypes.NpgsqlDbType.Varchar, filtroDatos[2].ToString().Trim());
            parametros.agregarParametro("@fechainicio", NpgsqlTypes.NpgsqlDbType.Timestamp, filtroDatos[3]);
            parametros.agregarParametro("@fechafin", NpgsqlTypes.NpgsqlDbType.Timestamp, filtroDatos[4]);
            
            DataSet datos = this.cnx.ejecutarConsultaSQL3(sql, parametros.obtenerParametros());
            this.Error = this.cnx.IsError;
            this.ErrorMsg = this.cnx.ErrorDescripcion;

            if (!this.Error)
            {
                foreach (DataRow fila in datos.Tables[0].Rows)
                {
                    TareaE oTareaE = new TareaE(
                                                   Int32.Parse(fila["id"].ToString()),
                                                   fila["descripcion"].ToString(),
                                                   fila["colaborador"].ToString(),
                                                   fila["estado"].ToString(),
                                                   fila["prioridad"].ToString(),
                                                   DateTime.Parse(fila["fechainicio"].ToString()),
                                                   DateTime.Parse(fila["fechafin"].ToString()),
                                                   fila["notas"].ToString()
                                                   );
                    tareas.Add(oTareaE);
                }
            }

            return tareas;              
        }




    }
}
