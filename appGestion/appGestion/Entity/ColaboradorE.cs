﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appGestion.Entity
{
    public class ColaboradorE
    {
        public int id { get; set; }
        public string codigo { get; set; }

        public string nombre { get; set; }

        public ColaboradorE() 
        {
            this.id = 0;
            this.codigo = "";
            this.nombre = "";
            this.codigo = "";
            this.nombre = "";

        }

        public ColaboradorE(int pId, string pCodigo, string pNombre) 
        {
            this.id = pId;
            this.codigo = pCodigo;
            this.nombre = pNombre;
        
        }

        public override string ToString()
        {
            return "\nId: " + this.id + "\n" +
                   "Codigo: " + this.codigo + "\n" +
                   "Nombre: " + this.nombre + "\n\n";
        }
        



    }
}
