﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appGestion.Entity
{
    public class TareaE
    {

        public int id { get; set; }
        public string descripcion { get; set; }
        public string colaborador { get; set; }
        public string estado { get; set; }
        public string prioridad { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public string notas { get; set; }


        public TareaE() 
        {
            this.id = 0;
            this.descripcion = "";
            this.colaborador = "";
            this.estado = "";
            this.prioridad = "";
            this.fechaInicio = DateTime.Now;
            this.fechaFin = DateTime.Now;
            this.notas = "";
        }

        public TareaE(int pId, string pDescripcion, string pColaborador, string pEstado, 
            string pPrioridad, DateTime pFechaInicio, DateTime pFechaFin, string pNotas)
        {
            this.id = pId;
            this.descripcion = pDescripcion;
            this.colaborador = pColaborador;
            this.estado = pEstado;
            this.prioridad = pPrioridad;
            this.fechaInicio = pFechaInicio;
            this.fechaFin = pFechaFin;
            this.notas = pNotas;
        }

        public TareaE(string pDescripcion, string pColaborador, string pEstado,
            string pPrioridad, DateTime pFechaInicio, DateTime pFechaFin, string pNotas)
        {
            this.descripcion = pDescripcion;
            this.colaborador = pColaborador;
            this.estado = pEstado;
            this.prioridad = pPrioridad;
            this.fechaInicio = pFechaInicio;
            this.fechaFin = pFechaFin;
            this.notas = pNotas;
        }


        public override string ToString()
        {
            return "\nId: " + this.id + "\n" +
                   "Descripción: " + this.descripcion + "\n" +
                   "Colaborador: " + this.colaborador + "\n" +
                   "Estado: " + this.estado + "\n" +
                   "Prioridad: " + this.prioridad + "\n" +
                   "Fecha inicio: " + this.fechaInicio + "\n" +
                   "Fecha fin: " + this.fechaFin + "\n" +
                   "Notas: " + this.notas + "\n\n";
        }




    }
}
