﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using appGestion.Data;
using appGestion.Entity;
using appGestion.GUI;
using System.Globalization;
using System.Runtime;



namespace appGestion.GUI
{
    public partial class frmTarea : Form
    {
        private List<ColaboradorE> oColaboradoresE;
        private List<TareaE> oTareasE;
        private bool validacionDatos;
        private String mensageValidacionDatos;
        private int idTareaModificar;
        public frmTarea()
        {
            InitializeComponent();
            this.oColaboradoresE = new List<ColaboradorE>();
            this.oTareasE = new List<TareaE>();
            this.validacionDatos = false;
            this.mensageValidacionDatos = "";
            this.llenarComboColaboradores();
            this.llenarComboEstado();
            this.llenarComboPrioridad();
            this.cargarDGVTareas();
            this.desHabilitarBotonGuardar();
            this.limpiar();
            
        }

        private void frmTarea_Load(object sender, EventArgs e)
        { }

        public void llenarComboColaboradores() 
        {
            ColaboradorD oColaboradorD = new ColaboradorD();
            this.oColaboradoresE = oColaboradorD.obtenerColaboradores();

            if (!oColaboradorD.Error)
            {
                foreach (ColaboradorE aux in this.oColaboradoresE)
                {
                    this.cmbColaboradores.Items.Add(aux.nombre);

                }
                this.cmbColaboradores.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show(oColaboradorD.ErrorMsg, "Error");
            }

        }

        public void llenarComboEstado() 
        {
            this.cmbEstado.Items.Clear();
            this.cmbEstado.Items.Add("PENDIENTE");
            this.cmbEstado.Items.Add("EN PROCESO");
            this.cmbEstado.Items.Add("FINALIZADA");
            this.cmbEstado.SelectedIndex = 0;
        }

        public void llenarComboEstadoModificar()
        {
            this.cmbEstado.Items.Clear();
            this.cmbEstado.Items.Add("PENDIENTE");
            this.cmbEstado.Items.Add("EN PROCESO");
            this.cmbEstado.Items.Add("FINALIZADA");
            this.cmbEstado.SelectedIndex = 0;
        }

        public void llenarComboPrioridad()
        {
            this.cmbPrioridad.Items.Add("ALTA");
            this.cmbPrioridad.Items.Add("MEDIA");
            this.cmbPrioridad.Items.Add("BAJA");
            this.cmbPrioridad.SelectedIndex = 0;
        }

        public void limpiar()
        {
            this.txtDescripcion.Text = "";
            this.txtNotas.Text = "";
            this.dtpFechaInicio.Value = DateTime.Now;
            this.dtpFechaFin.Value = DateTime.Now;
            this.validacionDatos = false;
            this.mensageValidacionDatos = "";
            this.idTareaModificar = 0;
            this.desHabilitarBotonGuardar();
            this.cbFechaInicio.Checked = true;
            this.cbFechaInicio.Enabled = false;
            this.cbFechaFin.Checked = true;
            this.cbFechaFin.Enabled = false;
        }

        public void desHabilitarBotonGuardar()
        {
            this.btnGuardar.Enabled = false;
        }

        public void habilitarBotonGuardar()
        {
            this.btnGuardar.Enabled = true;
        }

        public bool validarDatos() 
        {
            DateTime fechaInicio = this.dtpFechaInicio.Value.Date;
            DateTime fechaFin = this.dtpFechaFin.Value.Date;
            DateTime fechaActual = DateTime.Now.Date;

            if (this.txtDescripcion.Text != "")
            {
                if (DateTime.Compare(fechaInicio, fechaActual) >= 0)
                {
                    if (DateTime.Compare(fechaInicio, fechaFin) <= 0)
                    {
                        this.validacionDatos = true;
                    }
                    else
                    {
                        this.mensageValidacionDatos = "Fecha de inicio debe ser igual o menor a la fecha final";
                    }
                    
                }
                else
                {
                    this.mensageValidacionDatos = "Fecha de inicio debe ser igual o mayor a la fecha actual";
                    this.validacionDatos = false;
                }
                
            }
            else
            {
                this.mensageValidacionDatos = "La descripción es necesaria";
                this.txtDescripcion.Text = "";
                this.validacionDatos = false;
            }

            return this.validacionDatos;
        }

        public void cargarDGVTareas() 
        {
            TareaD oTareaD = new TareaD();
            oTareasE = oTareaD.obtenerTareas();

            if (oTareasE.Count > 0)
            {
                this.dgvTareas.DataSource = null;
                this.dgvTareas.DataSource = oTareasE;
                DataGridViewColumn colunmaId = this.dgvTareas.Columns[0];
                colunmaId.Visible = false;
                this.dgvTareas.Columns[1].HeaderText = "DESCRIPCIÓN";
                this.dgvTareas.Columns[2].HeaderText = "COLABORADOR";
                this.dgvTareas.Columns[3].HeaderText = "ESTADO";
                this.dgvTareas.Columns[4].HeaderText = "PRIORIDAD";
                this.dgvTareas.Columns[5].HeaderText = "F. INICIO";
                this.dgvTareas.Columns[6].HeaderText = "F. FIN";
                DataGridViewColumn colunmaNotas = this.dgvTareas.Columns[7];
                colunmaNotas.Visible = false;

            }
            
        
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {

            if (this.validarDatos())
            {
                string descripcion = this.txtDescripcion.Text;
                string colaborador =  this.oColaboradoresE[cmbColaboradores.SelectedIndex].codigo;
                string estado = this.cmbEstado.SelectedItem.ToString();
                string prioridad = this.cmbPrioridad.SelectedItem.ToString();
                DateTime fechaInicio = this.dtpFechaInicio.Value;
                DateTime fechaFin = this.dtpFechaFin.Value;
                string notas = this.txtNotas.Text;
                TareaE oTareE = new TareaE(descripcion, colaborador, estado, prioridad, fechaInicio,
                    fechaFin, notas);
                TareaD oTareaD = new TareaD();
                oTareaD.crearTarea(oTareE);
                MessageBox.Show("Tarea creada con éxito", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.cargarDGVTareas();
                this.limpiar();
            }
            else {
           
                MessageBox.Show(this.mensageValidacionDatos.ToString(), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
         
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            TareaD oTareaD = new TareaD();
            List<TareaE> tareas = new List<TareaE>();
            tareas = oTareaD.obtenerTareas();
            int index = 0;
            TareaE tareaEliminar = new TareaE();


            if (tareas.Count > 0)
            {
                try
                {
                    index = this.dgvTareas.CurrentRow.Index;
                    tareaEliminar = (TareaE)tareas[index];
                }
                catch
                {  
                    MessageBox.Show("Debe seleccionar una fila de la tabla", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }                

                if (tareaEliminar.estado != "EN PROCESO")
                {
                    DialogResult respuesta =
                    MessageBox.Show("¿Está seguro de borrar la tarea?",
                                "Confirmación",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                    if (respuesta == DialogResult.Yes)
                    {

                        oTareaD.borrarTarea(tareaEliminar);
                        this.limpiar();
                        this.cargarDGVTareas();
                    }
                }
                else {
                    MessageBox.Show("Las tareas en proceso no se pueden eliminar", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else {
                MessageBox.Show("No existen tareas para eliminar", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            this.llenarComboEstadoModificar();
            TareaD oTareaD = new TareaD();
            List<TareaE> tareas = new List<TareaE>();
            //tareas = oTareaD.obtenerTareas();
            tareas = this.oTareasE;
            int indexTarea = 0;
            int indexColaborador = 0;
            int indexEstado = 0;
            int indexPrioridad = 0;
            TareaE tareaModificar = new TareaE();
            
            
            if (tareas.Count > 0)
            {
                try
                {
                    indexTarea = this.dgvTareas.CurrentRow.Index;
                    tareaModificar = (TareaE)tareas[indexTarea];
                    this.idTareaModificar = tareaModificar.id;

                    if (tareaModificar.estado != "FINALIZADA")
                    {
                        this.habilitarBotonGuardar();

                        indexColaborador = this.cmbColaboradores.Items.IndexOf(tareaModificar.colaborador);
                        this.cmbColaboradores.SelectedIndex = indexColaborador;

                        this.txtDescripcion.Text = tareaModificar.descripcion;

                        indexEstado = this.cmbEstado.Items.IndexOf(tareaModificar.estado);
                        this.cmbEstado.SelectedIndex = indexEstado;

                        indexPrioridad = this.cmbPrioridad.Items.IndexOf(tareaModificar.prioridad);
                        this.cmbPrioridad.SelectedIndex = indexPrioridad;

                        this.txtNotas.Text = tareaModificar.notas;

                        this.dtpFechaInicio.Value = tareaModificar.fechaInicio;
                        this.dtpFechaFin.Value = tareaModificar.fechaFin;


                    }
                    else
                    {
                        this.limpiar();
                        this.llenarComboEstado();                      
                        MessageBox.Show("Las tareas finalizadas no se pueden editar", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch
                {
                    this.llenarComboEstado();
                    MessageBox.Show("Debe seleccionar una fila de la tabla", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else
            {
                this.llenarComboEstado();
                MessageBox.Show("No existen tareas para modificar", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
 
            if (this.validarDatos())
            {
                string descripcion = this.txtDescripcion.Text;
                string colaborador = this.oColaboradoresE[cmbColaboradores.SelectedIndex].codigo;
                string estado = this.cmbEstado.SelectedItem.ToString();
                string prioridad = this.cmbPrioridad.SelectedItem.ToString();
                DateTime fechaInicio = this.dtpFechaInicio.Value;
                DateTime fechaFin = this.dtpFechaFin.Value;
                string notas = this.txtNotas.Text;
                TareaE oTareE = new TareaE(this.idTareaModificar, descripcion, colaborador, estado, prioridad, fechaInicio,
                    fechaFin, notas);
                TareaD oTareaD = new TareaD();
                oTareaD.modificarTarea(oTareE);
                MessageBox.Show("Tarea modificada con éxito", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.cargarDGVTareas();
                this.limpiar();
            }
            else
            {
                MessageBox.Show(this.mensageValidacionDatos.ToString(), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void btnFiltro_Click(object sender, EventArgs e)
        {
            string colaborador = "";
            string estado = "";
            string prioridad = "";
            DateTime fechaInicio = this.dtpFechaInicio.Value;
            DateTime fechaFin = this.dtpFechaFin.Value;
            object[] filtroDatos = new object[5];
            TareaD oTareaD = new TareaD();

            if (this.cbTodos.Checked != true)
            {
                if (this.cbColaborador.Checked == true)
                {    
                    colaborador = this.cmbColaboradores.SelectedItem.ToString();
            
                }
                if (this.cbEstado.Checked == true)
                {
                    estado = this.cmbEstado.SelectedItem.ToString();
                 
                }
                if (this.cbPrioridad.Checked == true)
                {
                    prioridad = this.cmbPrioridad.SelectedItem.ToString();
                   
                }
                filtroDatos[0] = colaborador;
                filtroDatos[1] = estado;
                filtroDatos[2] = prioridad;
                filtroDatos[3] = fechaInicio;
                filtroDatos[4] = fechaFin;
                List<TareaE> tareas = new List<TareaE>();
                //MessageBox.Show(filtroDatos[0].ToString());
                tareas = oTareaD.filtrarTareas(filtroDatos);
                this.cargarDGVTareasFiltro(tareas);
            }
            else {
         
                this.cargarDGVTareas();
            }

        }

        public void cargarDGVTareasFiltro(List<TareaE> pTareas)
        {
            
            if (pTareas.Count > 0)
            {
                this.oTareasE = pTareas;
                this.dgvTareas.DataSource = null;
                this.dgvTareas.DataSource = pTareas;
                DataGridViewColumn colunmaId = this.dgvTareas.Columns[0];
                colunmaId.Visible = false;
                this.dgvTareas.Columns[1].HeaderText = "DESCRIPCIÓN";
                this.dgvTareas.Columns[2].HeaderText = "COLABORADOR";
                this.dgvTareas.Columns[3].HeaderText = "ESTADO";
                this.dgvTareas.Columns[4].HeaderText = "PRIORIDAD";
                this.dgvTareas.Columns[5].HeaderText = "F. INICIO";
                this.dgvTareas.Columns[6].HeaderText = "F. FIN";
                DataGridViewColumn colunmaNotas = this.dgvTareas.Columns[7];
                colunmaNotas.Visible = false;
            }
            

        }







    }
}
